﻿Borland Pascal
==============
* Для запуска среды Borland Pascal служит ярлык "Pascal". В каталогах bp и dos находятся служебные файлы, их трогать не нужно.
* Свои программы можно сохранять в каталоге work.
* Для перехода на русский язык и обратно нужно нажать правый Ctrl.
