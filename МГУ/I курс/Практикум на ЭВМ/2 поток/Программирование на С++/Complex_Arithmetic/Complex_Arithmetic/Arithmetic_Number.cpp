#include "Arithmetic_Number.h"

Arithm_Number :: Arithm_Number ()
{
	Clean_Number ();
}

void Arithm_Number ::Clean_Number ()
{
	for (int i = 0; i < max_size_; i++)
	{
		number_ [i] = 0;
	}
	size_ = 0;
}

void Arithm_Number :: Read (char number []) /*	"0000123" = '32100000...'	*/
{
	this -> Clean_Number ();
	int i = 0;
	while (number [i] != '\0')	i++;
	i--;

	for (int j = 0; i >= 0; i--, j++)
	{
		number_[j] = number[i] - '0';
		if (number [i] != '0')
		{
			size_ = j +1;
		}
	}
}

void Arithm_Number :: Print ()
{
	for (int i = size_ - 1; i >= 0 ; i--)
		printf ("%d", number_[i]);
	printf ("\n");
}