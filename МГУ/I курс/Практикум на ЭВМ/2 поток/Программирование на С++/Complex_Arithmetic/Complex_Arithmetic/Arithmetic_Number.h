#include <stdio.h>

class Arithm_Number		/*	123 = "321000000..."	*/
{
		public:
	static const int max_size_ = 200;
	int size_;
	int number_ [max_size_];

		public:
	Arithm_Number ();
	void Clean_Number ();
	void Read (char number []);
	void Print ();
};