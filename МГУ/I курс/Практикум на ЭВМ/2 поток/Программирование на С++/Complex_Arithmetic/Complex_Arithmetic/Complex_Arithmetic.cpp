#include <conio.h>
#include "Arithmetic_Number.h"

void Sum (Arithm_Number* number, Arithm_Number num1, Arithm_Number num2, int degree = 0);
void Product (Arithm_Number* number, Arithm_Number num1, Arithm_Number num2);
Arithm_Number Special_expression (Arithm_Number number, int inumber); /* answer = number * (1 + (number+1) * (1 + ...*(1 + 100))...)) */

int main ()
{
	Arithm_Number number1;
	Arithm_Number number2;

	number1.Read ("2");
	number2.Read ("2");
	printf ("2^500 = ");
	for (int i = 1; i < 500; i++)
		Product (&number1, number1, number2);
	number1.Print ();
	printf ("\n");

	number1.Read ("1");
	number2 = Special_expression (number1, 1);
	printf ("1! + 2! + ... + 100! = ");
	number2.Print ();

	getch ();
	return 0;
}

//===========================================================================================================================
//===========================================================================================================================
//===========================================================================================================================
Arithm_Number Special_expression (Arithm_Number number, int inumber)
{
	Arithm_Number number2;
	number2.Read ("1");
	Arithm_Number number3;

	Sum (&number3, number, number2);
	if (inumber +1 < 100) number3 = Special_expression (number3, inumber + 1);
	Sum (&number3, number3, number2);
	Product (&number, number, number3);

	return number;
}


void Sum (Arithm_Number* number, Arithm_Number num1, Arithm_Number num2, int degree) /* number = num1 + num2 * degree*/
{
	number->Clean_Number ();
	int num = 0;
	for (int i = 0; i < num1.size_ || i < num2.size_ + degree; i++)
	{
		int number2 = 0;
		if (i >= degree) number2 = num2.number_[i - degree];
		num = num1.number_[i] + number2 + num;
		number->number_[i] = num % 10;
		num = (num - num % 10) / 10;
		number->size_ ++;
	}

	if (num != 0)
	{
		number->number_[number->size_] = num;
		number->size_++;
	}
}

void Product (Arithm_Number* number, Arithm_Number num1, Arithm_Number num2)
{
	number->Clean_Number ();
	Arithm_Number interm_number;
	for (int j = 0, degree = 0; j < num2.size_; j++, degree++)
	{
		interm_number.Clean_Number ();
		int num = 0;
		for (int i = 0; i < num1.size_; i++)
		{
			num = num1.number_[i] * num2.number_[j] + num;
			interm_number.number_[i] = num % 10;
			num = (num - num % 10) / 10;
			if (i >= interm_number.size_) interm_number.size_++;
		}
		if (num != 0)
		{
			interm_number.number_[interm_number.size_] = num;
			interm_number.size_++;
		}
		Sum (number, *number, interm_number, degree);
	}
}