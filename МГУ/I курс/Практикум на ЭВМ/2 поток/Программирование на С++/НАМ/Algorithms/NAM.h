#include <stdio.h>
#include <string.h>

class Substitution_formula
{
		public:
	static const int size_ = 102;
	char pattern_ [size_];
	char replacement_ [size_];
	bool stop_formula_;
		
		public:
	Substitution_formula ();
};

class NAM
{
		private:
	static const int max_data_size_ = 2002;
	static const int max_formulas_num_ = 100;
	int formulas_num_;
	char data_ [max_data_size_];
	Substitution_formula formulas_ [max_formulas_num_];

		public:
	NAM ();
	void Run ();
	void Processing_data ();
	void strinclude (int first_symbol, int formula_num);
	void Read_algorithm ();
};

//==============================================================================================================
//===================================Constructors===============================================================
//==============================================================================================================
Substitution_formula :: Substitution_formula ():
stop_formula_ (0)
{}

NAM :: NAM ():
formulas_num_ (max_formulas_num_)
{}

//===================================Functions==================================================================

void NAM :: Run ()
{
	printf ("Requirements:\n"
			"On first right, how many formulas will be in program.\n\n"
			"Every formula right on new line.\n\n"
			"Max number of substitution formulas < %d.\n\n"
			"Max number of symbols in formulas < %d.\n\n"
			"Max number of symbols in data < %d.\n\n"
			"\"_\" - empty symbol, \" - \" - pointer, \" | \" - stop pointer.\n\n"
			"In right and left part of substitution formulas should be only empty symbol\nor no empty symbols at all.\n\n"
			"NAM program must be in algorithm.txt.\n\n\n",
			formulas_num_, formulas_[0].size_ -2, max_data_size_ - 2);
	getch ();

	Read_algorithm ();
	printf ("\nEnter please data:\n");
	scanf ("%s", data_ +1); data_[0] = ' ';
	data_ [strlen (data_) +1] = data_ [strlen (data_)];
	data_ [strlen (data_)] = ' ';
	printf ("\n");

	Processing_data ();
	printf ("\nresult: %s", data_);
}

void NAM :: Processing_data ()
{
	for (int i = 0; i < formulas_num_; i++)
	{
		char* position = strstr (data_, formulas_[i].pattern_);
		if (position == NULL) continue;
		int pos = position - data_;
		printf ("%3d : %15s ", i+1, data_);
		if (formulas_[i].stop_formula_ == 1) printf ("|\32 ");
		else printf (" \32 ");
		strinclude (pos, i);
		printf ("%-15s\n", data_);
		if (formulas_[i].stop_formula_ == 1) break;
		i = -1;
	}
}

void NAM :: strinclude (int first_symbol, int formula_num)
{
	int length1 = strlen (formulas_ [formula_num].pattern_);
	int length2 = strlen (formulas_ [formula_num].replacement_);
	if (strlen (data_) - length1 + length2 > max_data_size_)
	{
		printf ("Too big data being processed.\n");
		throw 13;
	}
	char remain_data_ [max_data_size_];
	strcpy (remain_data_, data_ + first_symbol + length1);
	strncpy (data_ + first_symbol, formulas_ [formula_num].replacement_, length2);
	strcpy (data_ + first_symbol + length2, remain_data_);

	if (data_[0] != ' ')
	{
		strcpy (remain_data_, data_);
		data_[0] = ' ';
		strcpy (data_ + 1, remain_data_);
	}
	if (data_[strlen (data_) -1] != ' ')
	{
		data_ [strlen (data_) +1] = data_ [strlen (data_)];
		data_ [strlen (data_)] = ' ';
	}
}

void NAM :: Read_algorithm ()
{
	FILE* subst_formulas; fopen_s (&subst_formulas, "algorithm.txt", "r");
	
	fscanf (subst_formulas, "%d", &formulas_num_);
	if (formulas_num_ >= max_formulas_num_)
	{
		printf ("Too many substitution formulas.\n");
		throw 13;
	}
	
	for (int i = 0; i < formulas_num_; i++)
	{
		fscanf (subst_formulas, "%s ", formulas_[i].pattern_);
		if (strcmp (formulas_[i].pattern_, "_") == 0) strcpy (formulas_[i].pattern_, " ");
		char stop_formula_ = '-'; fscanf (subst_formulas, "%c", &stop_formula_);
		if (stop_formula_ == '|') formulas_[i].stop_formula_ = 1;
		fscanf (subst_formulas, " %s", formulas_[i].replacement_);
		if (strcmp (formulas_[i].replacement_, "_") == 0) strcpy (formulas_[i].replacement_, "");
	}

	fclose (subst_formulas); subst_formulas = NULL;

	printf ("Reading successfully completed.\nAlgorithm:\n");
	for (int i = 0; i < formulas_num_; i++)
	{
		printf ("%3d%10s ", i+1, formulas_[i].pattern_);
		if (formulas_[i].stop_formula_ == 1) printf ("|\32 ");
		else printf (" \32 ");
		printf (" %s\n", formulas_[i].replacement_);
	}
}